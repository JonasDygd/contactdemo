﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contact.Web.Server.Dto
{
    /// <summary>
    /// Extension methods for <see cref="Entities.Person"/> and <see cref="Person"/>
    /// </summary>
    public static class ExtensionOfPerson
    {
        /// <summary>
        /// Converts the person entity to a corresponding DTO
        /// </summary>
        /// <param name="person">The person entity</param>
        /// <returns>A person DTO</returns>
        public static Person ToDto(this Entities.Person person)
        {
            if (person == null)
                throw new ArgumentNullException(nameof(person));

            var targetPerson = new Person();

            person.CopyTo(targetPerson);

            return targetPerson;
        }

        /// <summary>
        /// Copies the person entity to a DTO
        /// </summary>
        /// <param name="sourcePerson">The person entity</param>
        /// <param name="targetPerson">The person DTO</param>
        public static void CopyTo(this Entities.Person sourcePerson, Person targetPerson)
        {
            if (sourcePerson == null)
                throw new ArgumentNullException(nameof(sourcePerson));

            if (targetPerson == null)
                throw new ArgumentNullException(nameof(targetPerson));

            targetPerson.Id = sourcePerson.Id;
            targetPerson.Name = sourcePerson.Name;
            targetPerson.BirthDate = sourcePerson.BirthDate;
            targetPerson.City = sourcePerson.City;
        }

        /// <summary>
        /// Converts the person DTO to a corresponding entity
        /// </summary>
        /// <param name="person">The person DTO</param>
        /// <returns>A person entity</returns>
        public static Entities.Person ToEntity(this Person person)
        {
            if (person == null)
                throw new ArgumentNullException(nameof(person));

            var targetPerson = new Entities.Person();

            person.CopyTo(targetPerson);

            return targetPerson;
        }

        /// <summary>
        /// Copies the person DTO to an entity
        /// </summary>
        /// <param name="sourcePerson">The person DTO</param>
        /// <param name="targetPerson">The person entity</param>
        /// <param name="skipId">If true, then the <see cref="Entities.Person.Id"/> property is skipped</param>
        public static void CopyTo(this Person sourcePerson, Entities.Person targetPerson, bool skipId = false)
        {
            if (sourcePerson == null)
                throw new ArgumentNullException(nameof(sourcePerson));

            if (targetPerson == null)
                throw new ArgumentNullException(nameof(targetPerson));

            if (!skipId)
                targetPerson.Id = sourcePerson.Id;
            
            targetPerson.Name = sourcePerson.Name;
            targetPerson.BirthDate = sourcePerson.BirthDate;
            targetPerson.City = sourcePerson.City;
        }
    }
}
