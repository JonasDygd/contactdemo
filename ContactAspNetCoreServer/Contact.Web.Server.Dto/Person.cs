﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Contact.Web.Server.Dto
{
    /// <summary>
    /// Represents a person
    /// </summary>
    public class Person
    {
        /// <summary>
        /// The ID of the person
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the person
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// The date the person was born
        /// </summary>
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// The city the person lives in
        /// </summary>
        [Required]
        public string City { get; set; }
    }
}
