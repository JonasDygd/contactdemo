﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contact.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Contact.Web.Server.Dto;
using System.ComponentModel.DataAnnotations;

namespace Contact.Web.Server.Controllers
{
    /// <summary>
    /// Provides a means for executing CRUD operations for <see cref="Dto.Person"/> 
    /// </summary>
    public class PersonController : ContactDbController
    {
        /// <summary>
        /// Creates an instance of <see cref="PersonController"/>
        /// </summary>
        /// <param name="dbContext">The Contact database context</param>
        /// <param name="logger">The logger</param>
        public PersonController(ContactDbContext dbContext, ILogger<PersonController> logger) 
            : base(dbContext, logger)
        {
        }

        /// <summary>
        /// Enumerates all persons
        /// </summary>
        /// <returns>An enumeration of all persons</returns>
        [HttpGet]
        public IEnumerable<Dto.Person> Get()
        {
            return DbContext.Persons
                            .OrderBy(p => p.Id)
                            .AsEnumerable()
                            .Select(p => p.ToDto());
        }

        /// <summary>
        /// Gets one specific person
        /// </summary>
        /// <param name="id">The ID of the person</param>
        /// <returns>That one specific person</returns>
        [HttpGet("{id}")]
        public ActionResult<Dto.Person> Get(int id)
        {
            Entities.Person dbPerson = DbContext.Persons.SingleOrDefault(p => p.Id == id);

            if (dbPerson == null)
                return PersonNotFound(logAsError: false);
            
            return dbPerson.ToDto();
        }

        /// <summary>
        /// Creates a new person
        /// </summary>
        /// <param name="person">The person to create</param>
        /// <returns>The result of the action</returns>
        [HttpPost]
        public IActionResult Post([FromBody] Dto.Person person)
        {
            if (!TryValidateModel(person))
                return ValidationProblem();

            Logger.LogInformation("Creating person named '{0}'...", person.Name);

            Entities.Person dbPerson = DbContext.Persons.SingleOrDefault(p => p.Name == person.Name);

            if (dbPerson == null)
                dbPerson = person.ToEntity();
            else
                return PersonAlreadyExists(dbPerson);

            DbContext.Persons.Add(dbPerson);

            SynchronizeDb();

            person.Id = dbPerson.Id;
            Logger.LogInformation("The person was created, and got assigned the ID {0}", person.Id);

            return CreatedAtAction(nameof(Get), new { id = person.Id }, person);
        }

        /// <summary>
        /// Updates a person
        /// </summary>
        /// <param name="id">The ID of the person to update</param>
        /// <param name="person">The new data for the person</param>
        /// <returns>The result of the action</returns>
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Dto.Person person)
        {
            if (!TryValidateModel(person))
                return ValidationProblem();

            Logger.LogInformation("Updating person with ID {0}...", id);

            Entities.Person dbPerson = DbContext.Persons.SingleOrDefault(p => p.Id == id);

            if (dbPerson == null)
                return PersonNotFound();

            Logger.LogInformation("Found person named '{0}'", dbPerson.Name);

            bool wasPersonRenamed = dbPerson.Name != person.Name;

            if (wasPersonRenamed)
            {
                Logger.LogInformation("The person will be renamed as '{0}'...", person.Name);

                Entities.Person dbOtherPerson = DbContext.Persons.SingleOrDefault(p => p.Name == person.Name);

                if (dbOtherPerson != null)
                    return PersonAlreadyExists(dbOtherPerson);
            }

            person.CopyTo(dbPerson, skipId: true);

            SynchronizeDb();
            Logger.LogInformation("The person was updated");

            return Ok();
        }

        /// <summary>
        /// Deletes a person
        /// </summary>
        /// <param name="id">The ID of the person to delete</param>
        /// <returns>The result of the action</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Logger.LogInformation("Deleting person with ID {0}...", id);

            Entities.Person dbPerson = DbContext.Persons.SingleOrDefault(p => p.Id == id);

            if (dbPerson == null)
                return PersonNotFound();

            Logger.LogInformation("Found person named '{0}'", dbPerson.Name);

            DbContext.Persons.Remove(dbPerson);

            SynchronizeDb();
            Logger.LogInformation("The person was deleted");

            return NoContent();
        }
        
        private IActionResult PersonAlreadyExists(Entities.Person dbPerson)
        {
            string message = string.Format("A person with the name '{0}' already exists in the database", dbPerson.Name);

            Logger.LogError(message);
            return BadRequest(message);
        }

        private NotFoundResult PersonNotFound(bool logAsError = true)
        {
            if (logAsError)
                Logger.LogError("No person was found");
            
            return NotFound();
        }
    }
}