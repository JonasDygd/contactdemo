﻿using Contact.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contact.Web.Server.Controllers
{
    /// <summary>
    /// Represents an abstract base class for a controller which uses a <see cref="ContactDbContext"/>
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public abstract class ContactDbController : ControllerBase
    {
        /// <summary>
        /// The Contact database context 
        /// </summary>
        protected ContactDbContext DbContext
        {
            get;
        }

        /// <summary>
        /// The logger
        /// </summary>
        protected ILogger Logger
        {
            get;
        }

        /// <summary>
        /// Creates an instance of <see cref="ContactDbController"/>
        /// </summary>
        /// <param name="dbContext">The Contact database context</param>
        /// <param name="logger">The logger</param>
        public ContactDbController(ContactDbContext dbContext, ILogger logger)
        {
            DbContext = dbContext;
            Logger = logger;
        }
        
        /// <summary>
        /// Saves the changes to the database
        /// </summary>
        protected void SynchronizeDb()
        {
            Logger.LogInformation("Synchronizing database...");
            DbContext.SaveChanges();
        }
    }
}
