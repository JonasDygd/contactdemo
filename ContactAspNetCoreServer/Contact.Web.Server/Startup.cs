using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contact.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore;
using Microsoft.OpenApi.Models;

namespace Contact.Web.Server
{
    /// <summary>
    /// Startup configuration for the ASP.NET Core application
    /// </summary>
    public class Startup
    {
        private readonly OpenApiInfo[] _openApiInfos;

        /// <summary>
        /// Creates an instance of <see cref="Startup"/>
        /// </summary>
        /// <param name="configuration">The configuration</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            _openApiInfos = new OpenApiInfo[]
            {
                new OpenApiInfo { Title = "Contact Demo API", Version = "v1" }
            };
        }

        /// <summary>
        /// The configuration
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Configures services
        /// </summary>
        /// <param name="services">The services</param>
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureDbContext(services);

            services.AddCors();
            services.AddControllers();

            ConfigureSwaggerGen(services);
        }

        /// <summary>
        /// Configures the application
        /// </summary>
        /// <param name="app">The application builder</param>
        /// <param name="env">The web host environment</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseCors(b => b.AllowAnyOrigin()
                              .AllowAnyMethod()
                              .AllowAnyHeader());

#if !DEBUG
            // Note: Firefox and ng-swagger-gen are both sensitive to self-signed SSL certificates, so I turned HTTP redirection
            // off in debug mode to avoid any such problems. Just so that the demo runs.

            app.UseHttpsRedirection();
#endif

            UseSwaggerAndSwaggerUI(app);

            app.UseRouting();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            EnsureDbCreated(app);
        }

        private void EnsureDbCreated(IApplicationBuilder app)
        {
            IServiceScopeFactory serviceScopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();

            using (IServiceScope scope = serviceScopeFactory.CreateScope())
            {
                using (ContactDbContext context = scope.ServiceProvider.GetService<ContactDbContext>())
                    context.Database.Migrate();
            }
        }

        private void ConfigureDbContext(IServiceCollection services)
        {
            services.AddDbContext<ContactDbContext>(o => 
            {
                string connectionString = Configuration.GetConnectionString(DbNames.ContactDemo);
                
                o.UseSqlServer(connectionString);
            });
        }

        private void ConfigureSwaggerGen(IServiceCollection services)
        {
            services.AddSwaggerGen(o =>
            {
                foreach (OpenApiInfo openApiInfo in _openApiInfos)
                    o.SwaggerDoc(openApiInfo.Version, openApiInfo);
            });
        }

        private void UseSwaggerAndSwaggerUI(IApplicationBuilder app)
        {
            // It appears ng-swagger-gen only works with V2
            app.UseSwagger(o => o.SerializeAsV2 = true);

            app.UseSwaggerUI(c =>
            {
                foreach (OpenApiInfo openApiInfo in _openApiInfos)
                {
                    string url = $"/swagger/{openApiInfo.Version}/swagger.json";

                    c.SwaggerEndpoint(url, openApiInfo.Title);
                }
            });
        }
    }
}
