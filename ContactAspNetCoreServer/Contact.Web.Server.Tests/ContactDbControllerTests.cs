using Contact.Entities;
using Contact.Web.Server.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;
using System;

namespace Contact.Web.Server.Tests
{
    public abstract class ContactDbControllerTests
    {
        private IServiceProvider _rootServiceProvider;

        protected IServiceProvider RootServiceProvider
        {
            get
            {
                if (_rootServiceProvider == null)
                    _rootServiceProvider = CreateServiceProvider();

                return _rootServiceProvider;
            }
        }

        protected IServiceScope Scope
        {
            get;
            set;
        }

        protected IServiceProvider ServiceProvider
        {
            get
            {
                return Scope?.ServiceProvider;
            }
        }

        [SetUp]
        public virtual void SetUp()
        {
            Scope = RootServiceProvider.CreateScope();
        }

        [TearDown]
        public virtual void TearDown()
        {
            Scope?.Dispose();
        }

        protected virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ContactDbContext>(o => o.UseInMemoryDatabase(DbNames.ContactDemo));
            
            services.AddSingleton<IHttpContextAccessor>(p => new HttpContextAccessor
            {
                HttpContext = new DefaultHttpContext { RequestServices = p }
            });
            
            services.AddControllers();            
            services.AddLogging(b => b.AddProvider(NullLoggerProvider.Instance));
        }

        protected void AddSpecificController<T>(IServiceCollection services) where T : ControllerBase
        {
            services.AddTransient(p =>
            {
                ControllerBase controller = p.CreateNewInstance<T>();

                controller.ControllerContext.HttpContext = p.GetRequiredService<IHttpContextAccessor>().HttpContext;
                controller.ObjectValidator = new CustomControllerModelValidator(controller, p);

                return (T)controller;
            });
        }

        protected void ResetDatabase()
        {
            ContactDbContext dbContext = ServiceProvider.CreateNewInstance<ContactDbContext>();

            dbContext.Database.EnsureDeleted();
        }

        private IServiceProvider CreateServiceProvider()
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            return serviceCollection.BuildServiceProvider();
        }
    }
}