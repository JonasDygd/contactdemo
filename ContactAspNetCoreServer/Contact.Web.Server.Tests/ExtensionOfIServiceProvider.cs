﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contact.Web.Server.Tests
{
    public static class ExtensionOfIServiceProvider
    {
        /// <summary>
        /// Creates a new instance of a type, regardless if (and how) it is registered with the service provider. Dependency injection will be used for the constructor
        /// </summary>
        /// <param name="serviceProvider">The service provider</param>
        /// <param name="type">The type of the class to create</param>
        /// <returns>An instance of the class specified by <see cref="type"/></returns>
        public static object CreateNewInstance(this IServiceProvider serviceProvider, Type type)
        {
            if (serviceProvider == null)
                throw new ArgumentNullException(nameof(serviceProvider));

            if (type == null)
                throw new ArgumentNullException(nameof(type));

            return ActivatorUtilities.CreateInstance(serviceProvider, type);
        }

        /// <summary>
        /// Creates a new instance of a type, regardless if (and how) it is registered with the service provider. Dependency injection will be used for the constructor
        /// </summary>
        /// <typeparam name="T">The type of the class to create</typeparam>
        /// <param name="serviceProvider">The service provider</param>
        /// <returns>An instance of the class specified by <see cref="T"/></returns>
        public static T CreateNewInstance<T>(this IServiceProvider serviceProvider)
        {
            return ActivatorUtilities.CreateInstance<T>(serviceProvider);
        }
    }
}
