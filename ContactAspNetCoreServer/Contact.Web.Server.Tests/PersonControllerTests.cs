using Contact.Web.Server.Controllers;
using NUnit.Framework;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Contact.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace Contact.Web.Server.Tests
{
    public class PersonControllerTests : ContactDbControllerTests
    {
        static class Names
        {
            public const string Name1 = "Anders Andersson";
            public const string Name2 = "Barbara Baily";
            public const string Name3 = "Chris Christensen";
            public const string Name4 = "Danny Davis";
        }

        static class BirthDates
        {
            public static DateTime Boomer = new DateTime(1960, 1, 1);
            public static DateTime GenerationX = new DateTime(1980, 2, 2);
            public static DateTime Millennial = new DateTime(1990, 3, 3);
        }

        static class Cities
        {
            public const string Hamburg = "Hamburg";
            public const string London = "London";
            public const string Stockholm = "Stockholm";
        }

        private PersonController PersonController
        {
            get;
            set;
        }

        private int InitialPersonCount
        {
            get;
            set;
        }

        public override void SetUp()
        {
            base.SetUp();

            ResetDatabase();
            InitialPersonCount = AddPersonsToDatabase();
            PersonController = ServiceProvider.GetRequiredService<PersonController>();
        }

        [Test]
        public void Get_all_persons()
        {
            Dto.Person[] persons = PersonController.Get()?.ToArray();

            Assert.NotNull(persons);
            Assert.AreEqual(3, persons.Length);

            Dto.Person person1 = persons.First();
            Assert.AreEqual(Names.Name1, person1.Name);
            Assert.AreEqual(BirthDates.Boomer, person1.BirthDate);
            Assert.AreEqual(Cities.Stockholm, person1.City);

            Dto.Person person2 = persons[1];
            Assert.AreEqual(Names.Name2, person2.Name);
            Assert.AreEqual(BirthDates.GenerationX, person2.BirthDate);
            Assert.AreEqual(Cities.London, person2.City);

            Dto.Person person3 = persons[2];
            Assert.AreEqual(Names.Name3, person3.Name);
            Assert.AreEqual(BirthDates.Millennial, person3.BirthDate);
            Assert.AreEqual(Cities.Hamburg, person3.City);
        }

        [Test]
        public void Get_one_person_that_exists()
        {
            ActionResult<Dto.Person> actionResult = PersonController.Get(1);

            Dto.Person person = actionResult.Value;

            Assert.NotNull(person);

            Assert.AreEqual(Names.Name1, person.Name);
            Assert.AreEqual(BirthDates.Boomer, person.BirthDate);
            Assert.AreEqual(Cities.Stockholm, person.City);
        }

        [Test]
        public void Get_one_person_that_doesnt_exist()
        {
            ActionResult<Dto.Person> actionResult = PersonController.Get(100);

            Assert.IsTrue(actionResult.Result is NotFoundResult);

            Dto.Person person = actionResult.Value;

            Assert.Null(person);
        }

        [Test]
        public void Post_person_successfully()
        {
            var person = new Dto.Person
            {
                Name = Names.Name4,
                BirthDate = BirthDates.Boomer,
                City = Cities.Stockholm
            };

            IActionResult actionResult = PersonController.Post(person);

            Assert.IsTrue(actionResult is CreatedAtActionResult);

            int currentPersonCount = GetCurrentPersonCount();
            Assert.AreEqual(InitialPersonCount + 1, currentPersonCount);
        }

        [Test]
        public void Post_person_without_name()
        {
            var person = new Dto.Person
            {
                BirthDate = BirthDates.Boomer,
                City = Cities.Stockholm
            };
            
            IActionResult actionResult = PersonController.Post(person);

            Assert.IsTrue(IsValidationProblem(actionResult));

            int currentPersonCount = GetCurrentPersonCount();
            Assert.AreEqual(InitialPersonCount, currentPersonCount);
        }

        [Test]
        public void Post_person_without_city()
        {
            var person = new Dto.Person
            {
                Name = Names.Name4,
                BirthDate = BirthDates.Boomer
            };

            IActionResult actionResult = PersonController.Post(person);

            Assert.IsTrue(IsValidationProblem(actionResult));

            int currentPersonCount = GetCurrentPersonCount();
            Assert.AreEqual(InitialPersonCount, currentPersonCount, $"{InitialPersonCount} != {currentPersonCount}");
        }        

        [Test]
        public void Post_person_with_taken_name()
        {
            var person = new Dto.Person
            {
                Name = Names.Name1,
                BirthDate = BirthDates.Boomer,
                City = Cities.Stockholm
            };

            IActionResult actionResult = PersonController.Post(person);

            Assert.IsTrue(actionResult is BadRequestObjectResult);

            int currentPersonCount = GetCurrentPersonCount();
            Assert.AreEqual(InitialPersonCount, currentPersonCount);
        }

        [Test]
        public void Put_person_successfully()
        {
            var person = new Dto.Person
            {
                Name = Names.Name4,
                BirthDate = BirthDates.GenerationX,
                City = Cities.London
            };

            int personId = 1;
            IActionResult actionResult = PersonController.Put(personId, person);

            Assert.IsTrue(actionResult is OkResult);

            ContactDbContext dbContext = ServiceProvider.CreateNewInstance<ContactDbContext>();
            int currentPersonCount = dbContext.Persons.Count();
            Assert.AreEqual(InitialPersonCount, currentPersonCount);

            Person dbPerson = dbContext.Persons.SingleOrDefault(p => p.Id == personId);
            
            Assert.IsNotNull(dbPerson);

            Assert.AreEqual(dbPerson.Name, dbPerson.Name);
            Assert.AreEqual(dbPerson.BirthDate, dbPerson.BirthDate);
            Assert.AreEqual(dbPerson.City, dbPerson.City);
        }

        [Test]
        public void Put_person_that_doesnt_exist()
        {
            var person = new Dto.Person
            {
                Name = Names.Name4,
                BirthDate = BirthDates.GenerationX,
                City = Cities.London
            };

            IActionResult result = PersonController.Put(100, person);

            Assert.IsTrue(result is NotFoundResult);
            
            int currentPersonCount = GetCurrentPersonCount();
            Assert.AreEqual(InitialPersonCount, currentPersonCount);
        }

        [Test]
        public void Put_person_without_name()
        {
            var person = new Dto.Person
            {
                BirthDate = BirthDates.Boomer,
                City = Cities.Stockholm
            };

            IActionResult actionResult = PersonController.Put(1, person);

            Assert.IsTrue(IsValidationProblem(actionResult));

            int currentPersonCount = GetCurrentPersonCount();
            Assert.AreEqual(InitialPersonCount, currentPersonCount);
        }

        [Test]
        public void Put_person_without_city()
        {
            var person = new Dto.Person
            {
                Name = Names.Name4,
                BirthDate = BirthDates.Boomer
            };

            IActionResult actionResult = PersonController.Post(person);

            Assert.IsTrue(IsValidationProblem(actionResult));

            int currentPersonCount = GetCurrentPersonCount();
            Assert.AreEqual(InitialPersonCount, currentPersonCount);
        }

        [Test]
        public void Put_person_with_taken_name()
        {
            var person = new Dto.Person
            {
                Name = Names.Name1,
                BirthDate = BirthDates.Boomer,
                City = Cities.Stockholm
            };

            IActionResult actionResult = PersonController.Post(person);

            Assert.IsTrue(actionResult is BadRequestObjectResult);

            int currentPersonCount = GetCurrentPersonCount();
            Assert.AreEqual(InitialPersonCount, currentPersonCount);
        }

        [Test]
        public void Delete_one_person_that_exists()
        { 
            IActionResult result = PersonController.Delete(1);

            Assert.IsTrue(result is NoContentResult);

            int currentPersonCount = GetCurrentPersonCount();
            Assert.AreEqual(InitialPersonCount - 1, currentPersonCount);
        }

        [Test]
        public void Delete_one_person_that_doesnt_exist()
        {
            IActionResult result = PersonController.Delete(100);

            Assert.IsTrue(result is NotFoundResult);
            
            int currentPersonCount = GetCurrentPersonCount();
            Assert.AreEqual(InitialPersonCount, currentPersonCount);
        }

        protected override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);

            AddSpecificController<PersonController>(services);
        }

        private int AddPersonsToDatabase()
        {
            ContactDbContext dbContext = ServiceProvider.CreateNewInstance<ContactDbContext>();

            dbContext.Persons.Add(new Person
            {
                Name = Names.Name1,
                BirthDate = BirthDates.Boomer,
                City = Cities.Stockholm
            });

            dbContext.Persons.Add(new Person
            {
                Name = Names.Name2,
                BirthDate = BirthDates.GenerationX,
                City = Cities.London
            });

            dbContext.Persons.Add(new Person
            {
                Name = Names.Name3,
                BirthDate = BirthDates.Millennial,
                City = Cities.Hamburg
            });

            int initialPersonCount = dbContext.Persons.Local.Count;

            dbContext.SaveChanges();

            return initialPersonCount;
        }

        private int GetCurrentPersonCount()
        {
            ContactDbContext dbContext = ServiceProvider.CreateNewInstance<ContactDbContext>();
            
            return dbContext.Persons.Count();
        }

        private bool IsValidationProblem(IActionResult actionResult)
        {
            if (actionResult is ObjectResult objectResult && objectResult.Value is ValidationProblemDetails)
                return true;

            return false;
        }
    }
}