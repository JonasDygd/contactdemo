﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Contact.Web.Server.Tests
{
    public class CustomControllerModelValidator : IObjectModelValidator
    {
        private readonly ControllerBase _controller;
        private readonly IServiceProvider _serviceProvider;

        public CustomControllerModelValidator(ControllerBase controller, IServiceProvider serviceProvider)
        {
            this._controller = controller ?? throw new ArgumentNullException(nameof(controller));
            this._serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
        }

        public void Validate(ActionContext actionContext, ValidationStateDictionary validationState, string prefix, object model)
        {
            var validationContext = new ValidationContext(model, serviceProvider: _serviceProvider, items: null);
            var validationResults = new List<ValidationResult>();

            _controller.ModelState.Clear();

            bool isValid = Validator.TryValidateObject(model, validationContext, validationResults);

            if (isValid)
                return;

            int validationIndex = 0;
            foreach (ValidationResult validationResult in validationResults)
            {
                string validationKey = $"ValidationError{++validationIndex}";
                _controller.ModelState.AddModelError(validationKey, validationResult.ErrorMessage);
            }
        }
    }
}
