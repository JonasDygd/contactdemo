﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;

namespace Contact.Entities
{
    /// <summary>
    /// Represents the Contact database context 
    /// </summary>
    public class ContactDbContext : DbContext
    {
        /// <summary>
        /// The persons in the database
        /// </summary>
        public DbSet<Person> Persons
        {
            get;
            set;
        }

        /// <summary>
        /// Creates an instance of <see cref="ContactDbContext"/>
        /// </summary>
        /// <param name="options">The options</param>
        public ContactDbContext(DbContextOptions options)
            : base(options)
        {
        }

        /// <inheritdoc />
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.RemovePluralizingTableNameConvention();

            modelBuilder.ApplyConfiguration(new PersonConfiguration());
        }
    }
}
