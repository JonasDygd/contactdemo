﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contact.Entities
{
    class PersonConfiguration : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            const int stringMaxLength = 150;

            builder.Property(e => e.Name).IsRequired()
                                         .HasMaxLength(stringMaxLength)
                                         .IsUnicode(false);

            builder.Property(e => e.City).IsRequired()
                                         .HasMaxLength(stringMaxLength)
                                         .IsUnicode(false);
        }
    }
}
