export const environment = {
  production: true,

  // This is for demo purposes and obviously not a real production URL
  restServiceBaseUrl: 'https://localhost:44325' 
};
