import {AfterContentInit, Directive, ElementRef} from '@angular/core';

@Directive({
  selector: 'input[appAutoFocus]'
})
export class InputAutoFocusDirective implements AfterContentInit {
  constructor(private elementRef: ElementRef<HTMLInputElement>) {
  }

  ngAfterContentInit(): void {
    this.elementRef.nativeElement.focus();
  }
}