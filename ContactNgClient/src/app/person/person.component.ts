import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonViewModel } from 'src/view-models/person-view-model';
import { PersonService } from 'src/services';
import { Person } from 'src/models';
import { BasicDateHelper } from 'src/helpers/basic-date-helper';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpErrorResponseHelper } from 'src/helpers/http-error-response-helper';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {

  personViewModel: PersonViewModel;
  isErroneousStatusMessage: boolean;
  statusMessage: string;
  isConfirmingDelete: boolean;

  get isNewPerson(): boolean {
    return this.personViewModel.id ? false : true;
  }

  constructor(private personService: PersonService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.personViewModel = new PersonViewModel();    
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      let id = +paramMap.get('id');

      if (id)
        this.loadPerson(id);      
    });
  }

  onSubmit() {    
     this.resetStatusMessage();

     let person: Person = {};
     this.personViewModel.copyTo(person);

     if (!this.saveExistingPerson(person))
        this.saveNewPerson(person);
  }  

  deletePerson() {
    this.resetStatusMessage();
    this.personService.deletePersonId(this.personViewModel.id)
                      .subscribe({
                        next: this.navigateToPersonList.bind(this),
                        error: this.handlePersonServiceError.bind(this)
                      });

  }

  updateBirthDate(birthDate: string) {
     this.personViewModel.birthDate = BasicDateHelper.stringToDate(birthDate);
  }

  resetStatusMessage() {
    this.setSuccessfulStatusMessage(null);
  }

  private saveExistingPerson(person: Person) : boolean {
    if (this.isNewPerson)
       return false;

    this.personService.putPersonId({ id: person.id, body: person })
                      .subscribe({
                          next: () => { 
                            this.loadPerson(person.id);
                            this.setSuccessfulStatusMessage('The person was saved');
                          },
                          error: this.handlePersonServiceError.bind(this)
                      });

    return true;
  }

  private saveNewPerson(person: Person) {      
    this.personService.postPerson(person)
                      .subscribe({
                           next: this.navigateToPersonList.bind(this),
                           error: this.handlePersonServiceError.bind(this)
                        });
  }

  private navigateToPersonList() {
    this.router.navigate(['/person-list']); 
  }

  private handlePersonServiceError(response: HttpErrorResponse) {
    let errorMessage = HttpErrorResponseHelper.getFirstErrorMessageOrDefault(response);

    this.setErroneousStatusMessage(errorMessage);  
  }

  private loadPerson(id: number) {
    this.personService.getPersonId(id)
                      .subscribe({
                        next: p => this.personViewModel.copyFrom(p),
                        error: this.handlePersonServiceError.bind(this)
                      });
  }

  private setSuccessfulStatusMessage(statusMessage) {
    this.isErroneousStatusMessage = false;
    this.statusMessage = statusMessage;
  }

  private setErroneousStatusMessage(statusMessage) {
    this.isErroneousStatusMessage = true;
    this.statusMessage = statusMessage;
  }
}
