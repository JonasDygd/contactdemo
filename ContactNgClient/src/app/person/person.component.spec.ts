// Leaving this commented out for now, because Karma 'This constructor was not compatible with Dependency Injection.'

/*
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonComponent } from './person.component';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { convertToParamMap } from '@angular/router';
import { of } from 'rxjs';

describe('PersonComponent', () => {
  let component: PersonComponent;
  let fixture: ComponentFixture<PersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonComponent ],
      providers: [ 
                   HttpClient, 
                   HttpHandler, 
                   Router,
                   {
                    provide: ActivatedRoute,
                    useValue: {
                      paramMap: of(convertToParamMap({ 
                        id: 1          
                      }))
                    }
                  } 
                ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
*/