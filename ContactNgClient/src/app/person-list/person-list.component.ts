import { Component, OnInit } from '@angular/core';
import { PersonService } from 'src/services';
import { PersonViewModel } from 'src/view-models/person-view-model';
import { HttpErrorResponseHelper } from 'src/helpers/http-error-response-helper';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.scss']
})
export class PersonListComponent implements OnInit {

  personViewModels: PersonViewModel[];
  errorMessage: string;
  isLoading: boolean;

  constructor(private personService: PersonService) { 
    this.personViewModels = null;
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.loadPersons();
  }

  loadPersons(): void {
    this.resetErrorMessage();

    this.isLoading = true;
    this.personService.getPerson()
                      .subscribe({ 
                          next: persons => {
                            this.personViewModels = persons.map(p => new PersonViewModel(p));
                            this.isLoading = false;
                          },
                          error: this.handlePersonServiceError.bind(this)
                      });
  }

  private handlePersonServiceError(response: HttpErrorResponse) {
    let errorMessage = HttpErrorResponseHelper.getFirstErrorMessageOrDefault(response);

    this.setErrorMessage(errorMessage);
  }

  private resetErrorMessage() {
    this.setErrorMessage(null);
  }

  private setErrorMessage(errorMessage) {
    this.errorMessage = errorMessage;
    this.isLoading = false;
  }
}
