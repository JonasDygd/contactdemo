export class NumberHelper {
    static padNumber(numberToPad: number, zeroDigitCount: number) : string {
        return numberToPad.toString()
                          .padStart(zeroDigitCount, '0');
    }
}