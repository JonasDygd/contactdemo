import { NumberHelper } from './number-helper';
import { BasicDateHelper } from './basic-date-helper';

export class RestServiceDateHelper {
    static stringToDate(str: string) : Date {
        return BasicDateHelper.stringToDate(str);
    }

    static dateToString(date: Date) {
        let year = date.getFullYear();
        let month = NumberHelper.padNumber(date.getMonth() + 1, 2);
        let dayOfMonth = NumberHelper.padNumber(date.getDate(), 2);
        
        return `${year}-${month}-${dayOfMonth}T00:00:00`;
    }
}