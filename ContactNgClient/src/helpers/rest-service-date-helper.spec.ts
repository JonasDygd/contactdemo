import { RestServiceDateHelper } from "./rest-service-date-helper";

describe('RestServiceDateHelper', () => {

    it('dateToString should format the date correctly', () => {
        let date = new Date(2000, 0, 2);
        let formattedDate: string = RestServiceDateHelper.dateToString(date);
    
        expect(formattedDate === '2000-01-02T00:00:00').toBeTruthy();
    });

});
