import { BasicDateHelper } from "./basic-date-helper";

describe('BasicDateHelper', () => {

    it('stringToDate should return minDate if string is not a date', () => {
        let date: Date = BasicDateHelper.stringToDate('ABCDEFGHIJ');
        let isMinDate: boolean = date.getTime() === BasicDateHelper.minDate.getTime();
    
        expect(isMinDate).toBeTruthy();
    });

    it('stringToDate should return minDate if string is shorter than 10 characters', () => {
        let date: Date = BasicDateHelper.stringToDate('2000-01-0');
        let isMinDate: boolean = date.getTime() === BasicDateHelper.minDate.getTime();
    
        expect(isMinDate).toBeTruthy();
    });

    it('stringToDate should successfully convert a string to date', () => {
        let date: Date = BasicDateHelper.stringToDate('2000-01-02');
        
        let isMinDate: boolean = date.getTime() === BasicDateHelper.minDate.getTime();
        let isYearOk: boolean = date.getFullYear() === 2000;
        let isMonthOk: boolean = date.getMonth() === 0;
        let isDayOfMonthOk: boolean = date.getDate() === 2;

        expect(!isMinDate && isYearOk && isMonthOk && isDayOfMonthOk).toBeTruthy();
    });

});
