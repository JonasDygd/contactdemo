export class BasicDateHelper {

    static get minDate(): Date {
        return new Date(-8640000000000000);
    }

    static stringToDate(str: string) : Date {
        if (str.length >= 10) {
            let year: number = +str.substr(0, 4);
            let firstSeparator = str.charAt(4);
            let month: number = +str.substr(5, 2) - 1;
            let secondSeparator = str.charAt(7);
            let dayOfMonth: number = +str.substr(8, 2);
        
            if (firstSeparator === '-' && firstSeparator === secondSeparator) {
               let date: Date = new Date(year, month, dayOfMonth);

               if (!isNaN(date.getTime()))
                   return new Date(year, month, dayOfMonth);
            }
        }

        return BasicDateHelper.minDate;
    }
}