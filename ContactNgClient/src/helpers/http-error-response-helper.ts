import { HttpErrorResponse } from '@angular/common/http';

export class HttpErrorResponseHelper {
    static getFirstErrorMessageOrDefault(response: HttpErrorResponse) : string {
       
        let error: any = response.error;

        if (error)
        {
           if (typeof error === 'string')
              return <string>response.error;

           if (error.errors) 
              return error.errors.Name[0];
        }
    
        return 'An error occurred when communicating with the server.'; 
    }
}