import { NumberHelper } from "./number-helper";

describe('NumberHelper', () => {

    it('padNumber should return the number 1 as \'01\' if the second argument is 2', () => {
        let paddedNumber: string = NumberHelper.padNumber(1, 2);
    
        expect(paddedNumber === '01').toBeTruthy();
    });

    it('padNumber should return the number 10 as \'10\' if the second argument is 2', () => {
        let paddedNumber: string = NumberHelper.padNumber(10, 2);
    
        expect(paddedNumber === '10').toBeTruthy();
    });

});
