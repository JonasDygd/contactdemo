/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Person } from '../models/person';
@Injectable({
  providedIn: 'root',
})
class PersonService extends __BaseService {
  static readonly getPersonPath = '/Person';
  static readonly postPersonPath = '/Person';
  static readonly getPersonIdPath = '/Person/{id}';
  static readonly putPersonIdPath = '/Person/{id}';
  static readonly deletePersonIdPath = '/Person/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @return Success
   */
  getPersonResponse(): __Observable<__StrictHttpResponse<Array<Person>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Person`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<Person>>;
      })
    );
  }
  /**
   * @return Success
   */
  getPerson(): __Observable<Array<Person>> {
    return this.getPersonResponse().pipe(
      __map(_r => _r.body as Array<Person>)
    );
  }

  /**
   * @param body undefined
   */
  postPersonResponse(body?: Person): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/Person`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param body undefined
   */
  postPerson(body?: Person): __Observable<null> {
    return this.postPersonResponse(body).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   * @return Success
   */
  getPersonIdResponse(id: number): __Observable<__StrictHttpResponse<Person>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Person/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Person>;
      })
    );
  }
  /**
   * @param id undefined
   * @return Success
   */
  getPersonId(id: number): __Observable<Person> {
    return this.getPersonIdResponse(id).pipe(
      __map(_r => _r.body as Person)
    );
  }

  /**
   * @param params The `PersonService.PutPersonIdParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `body`:
   */
  putPersonIdResponse(params: PersonService.PutPersonIdParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/Person/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `PersonService.PutPersonIdParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `body`:
   */
  putPersonId(params: PersonService.PutPersonIdParams): __Observable<null> {
    return this.putPersonIdResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  deletePersonIdResponse(id: number): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/Person/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  deletePersonId(id: number): __Observable<null> {
    return this.deletePersonIdResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module PersonService {

  /**
   * Parameters for putPersonId
   */
  export interface PutPersonIdParams {
    id: number;
    body?: Person;
  }
}

export { PersonService }
