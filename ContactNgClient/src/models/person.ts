/* tslint:disable */
export interface Person {
  id?: number;
  name?: string;
  birthDate?: string;
  city?: string;
}
