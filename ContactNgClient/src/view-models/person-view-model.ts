import { Person } from 'src/models';
import { RestServiceDateHelper } from 'src/helpers/rest-service-date-helper';

export class PersonViewModel {
    id?: number;
    name: string;
    birthDate: Date;
    city: string;

    constructor(person?: Person) {
      if (person)
        this.copyFrom(person);
      else
        this.birthDate = new Date(1950, 0, 1);
    }

    copyFrom(person: Person) {
      this.id = person.id;
      this.name = person.name;
      this.birthDate = RestServiceDateHelper.stringToDate(person.birthDate);
      this.city = person.city;
    }

    copyTo(person: Person) {
      person.id = this.id;
      person.name = this.name;
      person.birthDate = RestServiceDateHelper.dateToString(this.birthDate);
      person.city = this.city;
    }
  }
  